import React from 'react';
import {View, Text, Image} from 'react-native';
import styles from './styles';

const details = (props) => {
  console.log('detail page data: ', props);
  const data = props.route.params;
  return (
    <View style={styles.container}>
      <Image style={styles.imageStyle} source={{uri: data.artworkUrl100}} />
      <Text style={styles.textStyle}>{data.trackName}</Text>
      <Text>{data.artistName}</Text>
    </View>
  );
};

export default details;
