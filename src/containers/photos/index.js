import React, {useEffect, useContext, useState, Fragment} from 'react';
import {View, Text, FlatList, Image, TouchableOpacity} from 'react-native';
import {LOADING_START} from '../../context/action/type';
import {Store} from '../../context/store';
import {sendGetRequest} from '../../network';
import styles from './styles';
import apiConstant from '../../network/Endpoint';

const Photos = (props) => {
  const globalState = useContext(Store);
  const {dispatchLoaderAction} = globalState;
  const [photos, setPhotos] = useState([]);

  useEffect(() => {
    dispatchLoaderAction({
      type: LOADING_START,
    });

    fetchPhotos();
  }, []);

  async function fetchPhotos() {
    const apiResult = await sendGetRequest(
      apiConstant.albumUrl,
      {},
      dispatchLoaderAction,
    );
    setPhotos(apiResult.results);
  }

  const photoItem = (item) => (
    <TouchableOpacity
      onPress={() => props.navigation.navigate('Details', item)}>
      <View style={styles.container}>
        <Image style={styles.imageStyle} source={{uri: item.artworkUrl100}} />
        <Text style={styles.textStyle}>{item.trackName}</Text>
      </View>
    </TouchableOpacity>
  );

  return (
    <Fragment>
      <FlatList
        data={photos}
        keyExtractor={(_, index) => index.toString()}
        numColumns={3}
        renderItem={({item}) => photoItem(item)}
      />
    </Fragment>
  );
};

export default Photos;
