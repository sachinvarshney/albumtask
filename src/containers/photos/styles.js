import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    width: 120,
    alignItems: 'center',
    margin: 5,
  },
  imageStyle: {
    height: 120,
    width: 110,
    resizeMode: 'contain',
  },
  textStyle: {
    alignSelf: 'center',
    textAlign: 'center',
  },
});
